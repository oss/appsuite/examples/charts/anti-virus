# anti-virus

![Version: 1.0.0](https://img.shields.io/badge/Version-1.0.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.0.0](https://img.shields.io/badge/AppVersion-1.0.0-informational?style=flat-square)

An example anti-virus Helm umbrella chart for Kubernetes

## Maintainers

| Name | Email | Url |
| ---- | ------ | --- |
| Sebastian Lutz | <sebastian.lutz@open-xchange.com> |  |

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| file://../c-icap | c-icap | 1.0.0 |
| https://charts.truecharts.org/ | clamav | 7.0.12 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| c-icap.clamav.host | string | `"clamav"` |  |
| c-icap.clamav.port | int | `3310` |  |
| c-icap.icap.port | int | `1344` |  |
| c-icap.icap.protocol | string | `"TCP"` |  |
| c-icap.image.pullPolicy | string | `"IfNotPresent"` |  |
| c-icap.image.repository | string | `"registry.gitlab.open-xchange.com/middleware/examples/images/c-icap"` |  |
| c-icap.image.tag | string | `"latest"` |  |
| c-icap.imagePullSecrets | list | `[]` |  |
| c-icap.podAnnotations | object | `{}` |  |
| c-icap.podLabels | object | `{}` |  |
| c-icap.replicaCount | int | `1` |  |
| c-icap.resources | object | `{}` |  |
| c-icap.service.port | int | `1344` |  |
| c-icap.service.type | string | `"ClusterIP"` |  |
| clamav.clamav.cron_enabled | bool | `true` |  |
| clamav.clamav.cron_schedule | string | `"* * * * *"` |  |
| clamav.clamav.date_format | string | `"+%m-%d-%Y_%H.%M.%S"` |  |
| clamav.clamav.extra_args | string | `""` |  |
| clamav.clamav.log_file_name | string | `"clamscan_report"` |  |
| clamav.clamav.report_path | string | `"/logs"` |  |
| clamav.cronjob.annotations | object | `{}` |  |
| clamav.cronjob.failedJobsHistoryLimit | int | `5` |  |
| clamav.cronjob.successfulJobsHistoryLimit | int | `2` |  |
| clamav.image.pullPolicy | string | `"IfNotPresent"` |  |
| clamav.image.repository | string | `"tccr.io/truecharts/clamav"` |  |
| clamav.image.tag | string | `"v1.2.1@sha256:f9fe7d0ccd86d1ee0583465a0ccf1cbd4bae7468c4c6297be4b8ca7f73260f42"` |  |
| clamav.persistence.logs.enabled | bool | `true` |  |
| clamav.persistence.logs.mountPath | string | `"/logs"` |  |
| clamav.persistence.logs.targetSelectAll | bool | `true` |  |
| clamav.persistence.scandir.enabled | bool | `true` |  |
| clamav.persistence.scandir.mountPath | string | `"/scandir"` |  |
| clamav.persistence.scandir.readOnly | bool | `true` |  |
| clamav.persistence.scandir.targetSelectAll | bool | `true` |  |
| clamav.persistence.sigdatabase.enabled | bool | `true` |  |
| clamav.persistence.sigdatabase.mountPath | string | `"/var/lib/clamav"` |  |
| clamav.persistence.sigdatabase.targetSelectAll | bool | `true` |  |
| clamav.portal.open.enabled | bool | `false` |  |
| clamav.securityContext.container.readOnlyRootFilesystem | bool | `false` |  |
| clamav.securityContext.container.runAsGroup | int | `0` |  |
| clamav.securityContext.container.runAsNonRoot | bool | `false` |  |
| clamav.securityContext.container.runAsUser | int | `0` |  |
| clamav.service.main.ports.main.port | int | `3310` |  |
| clamav.service.main.ports.main.protocol | string | `"http"` |  |
| clamav.service.main.ports.main.targetPort | int | `3310` |  |
| clamav.service.milter.enabled | bool | `true` |  |
| clamav.service.milter.ports.milter.enabled | bool | `true` |  |
| clamav.service.milter.ports.milter.port | int | `7357` |  |
| clamav.service.milter.ports.milter.protocol | string | `"http"` |  |
| clamav.service.milter.ports.milter.targetPort | int | `7357` |  |
| clamav.workload.main.podSpec.containers.main.env.CLAMAV_NO_CLAMD | bool | `false` |  |
| clamav.workload.main.podSpec.containers.main.env.CLAMAV_NO_FRESHCLAMD | bool | `false` |  |
| clamav.workload.main.podSpec.containers.main.env.CLAMAV_NO_MILTERD | bool | `true` |  |
| clamav.workload.main.podSpec.containers.main.env.CLAMD_STARTUP_TIMEOUT | int | `1800` |  |
| clamav.workload.main.podSpec.containers.main.env.FRESHCLAM_CHECKS | int | `1` |  |
| clamav.workload.main.podSpec.containers.main.probes.liveness.command[0] | string | `"clamdcheck.sh"` |  |
| clamav.workload.main.podSpec.containers.main.probes.liveness.enabled | bool | `true` |  |
| clamav.workload.main.podSpec.containers.main.probes.liveness.type | string | `"exec"` |  |
| clamav.workload.main.podSpec.containers.main.probes.readiness.command[0] | string | `"clamdcheck.sh"` |  |
| clamav.workload.main.podSpec.containers.main.probes.readiness.enabled | bool | `true` |  |
| clamav.workload.main.podSpec.containers.main.probes.readiness.type | string | `"exec"` |  |
| clamav.workload.main.podSpec.containers.main.probes.startup.command[0] | string | `"clamdcheck.sh"` |  |
| clamav.workload.main.podSpec.containers.main.probes.startup.enabled | bool | `true` |  |
| clamav.workload.main.podSpec.containers.main.probes.startup.type | string | `"exec"` |  |

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.11.3](https://github.com/norwoodj/helm-docs/releases/v1.11.3)
