# Open-Xchange Anti-Virus Example Helm Charts

This repository provides an umbrella Helm chart that consists of a ClamAV chart as well as a c-icap chart.

## Prerequisites

- Helm
- Kubernetes

## Documentation

Helm documentation can be found in the README of each Helm chart:

- [anti-virus](charts/anti-virus)
- [c-icap](charts/c-icap)


## Usage

```bash
helm repo add truecharts https://charts.truecharts.org/
helm dependency update charts/anti-virus/
helm upgrade --install anti-virus-example charts/anti-virus/
```

## License

This project uses the following license: Apache-2.0